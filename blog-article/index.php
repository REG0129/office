<!DOCTYPE html>
<html>
  <head>
    <title>fujioka-office</title>
    <meta charset="utf-8">
    <!--viewport-->
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--OGP common setting-->
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:url" content="******">
    <meta property="og:title" content="fujioka-office">
    <meta property="og:type" content="website">
    <meta property="og:description" content="藤岡事務所のオフィシャルサイトです。">
    <meta property="og:image" content="******">
    <!--.css road-->
    <link href="../stylesheet.min.css" type="text/css" rel="stylesheet">
    <!--JQuery road-->
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <!--JQuery UI road-->
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
  </head>

  <body>
    <header>
      <div class="header-bgcolor">
        <div class="l-header-top">
          <?php include("../php/header-top.php"); ?>
        </div>
      </div>
    </header>

    <main>
      <section class="p-blog-article">
        <div class="blog-article-contents">
          <div class="large-heading">
            <time datetime="2019-11-19">2019/11/19</time>

            <p class="large-heading-title">タイトルタイトルタイトルタイトルタイト
              ルタイトルタイトルタイトルタイトルタイトル</p>

            <a href="#"><figure><img src="../img/blog-article/it.jpg" srcset="../img/blog-article/it.jpg 1x,../img/blog-article/it@2x.jpg 2x" alt="it"></figure></a>

            <div class="c-text-box">
              <p>テキストですダミーテキストです。テキストですダミーテキストです。テキ
                ストですダミーテキストです。テキストですダミーテキストです。テキスト
                ですダミーテキストです。テキストですダミーテキストです。テキストです
                ダミーテキストです。</p>
            </div>
          </div>

          <div class="middle-heading">
            <p class="middle-heading-title">タイトル中見出し</p>

            <div class="c-text-box">
              <p>テキストですダミーテキストです。テキストですダミーテキストです。テキ
                ストですダミーテキストです。テキストですダミーテキストです。テキスト
                ですダミーテキストです。テキストですダミーテキストです。テキストです
                ダミーテキストです。</p>
              <br>
              <br>
              <br>
              <p>テキストですダミーテキストです。テキストですダミーテキストです。テキ
                ストですダミーテキストです。テキストですダミーテキストです。テキスト
                ですダミーテキストです。テキストですダミーテキストです。テキストです
                ダミーテキストです。テキストですダミーテキストです。テキストですダミ
                ーテキストです。テキストですダミーテキストです。テキストですダミーテ
                キストです。テキストですダミーテキストです。テキストですダミーテキス
                トです。テキストですダミーテキストです。テキストですダミーテキストで
                す。テキストですダミーテキストです。テキストですダミーテキストです。
                テキストですダミーテキストです。テキストですダミーテキストです。テキ
                ストですダミーテキストです。テキストですダミーテキストです。</p>
            </div>
          </div>

          <div class="small-heading">
            <p class="small-heading-title">タイトル小見出し</p>

            <div class="c-text-box">
              <p>テキストですダミーテキストです。テキストですダミーテキストです。テキ
                ストですダミーテキストです。テキストですダミーテキストです。テキスト
                ですダミーテキストです。テキストですダミーテキストです。テキストです
                ダミーテキストです。テキストですダミーテキストです。テキストですダミ
                ーテキストです。テキストですダミーテキストです。テキストですダミーテ
                キストです。テキストですダミーテキストです。テキストですダミーテキス
                トです。テキストですダミーテキストです。テキストですダミーテキストで
                す。テキストですダミーテキストです。テキストですダミーテキストです。
                テキストですダミーテキストです。テキストですダミーテキストです。テキ
                ストですダミーテキストです。テキストですダミーテキストです。</p>
            </div>

            <a href="#"><figure><img src="../img/blog-article/it.jpg" srcset="../img/blog-article/it.jpg 1x,../img/blog-article/it@2x.jpg 2x" alt="it"></figure></a>
          </div>

          <div class="blog-article-share">
            <a href="#"><figure><img src="../img/blog-article/it.jpg" srcset="../img/blog-article/it.jpg 1x,../img/blog-article/it@2x.jpg 2x" alt="it"></figure></a>

            <div class="share-type">
              <div class="facebook">
                <a>Facebookでシェアする</a>
              </div>

              <div class="twitter">
                <a>Twitterでこの記事をつぶやく</a>
              </div>
            </div>
          </div>
        </div>

        <div class="blog-article-latest">
          <p>最近の記事</p>

          <ul class="blog-article-latest-inner">
            <li class="c-blog-module">
              <?php include("../php/blog-article-20191111.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191110.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191109.php"); ?>
            </li>
          </ul>
        </div>
      </section>

      <section class="p-contact" style="background-color:#FAFAFA;">
        <?php include("../php/contact.php"); ?>
      </section>
    </main>

    <footer>
      <?php include("../php/footer.php"); ?>
    </footer>
  </body>
</html>
