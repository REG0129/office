<!DOCTYPE html>
<html>
  <head>
    <title>fujioka-office</title>
    <meta charset="utf-8">
    <!--viewport-->
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--OGP common setting-->
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:url" content="******">
    <meta property="og:title" content="fujioka-office">
    <meta property="og:type" content="website">
    <meta property="og:description" content="藤岡事務所のオフィシャルサイトです。">
    <meta property="og:image" content="******">
    <!--.css road-->
    <link href="../stylesheet.min.css" type="text/css" rel="stylesheet">
    <!--JQuery road-->
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <!--JQuery UI road-->
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
    <!--ggmap road-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzvCwkJdwx4Rl4uDuxP6Lzgtfgs0kkptM&callback=initMap" async defer></script>
    <script type="text/javascript" src="../js/ggmap.js"></script>
  </head>

  <body>
    <header>
      <div class="l-header-top">
        <?php include("../php/header-top.php"); ?>
      </div>

      <div class="about-header-main">
        <div class="c-header-title">
          <h3>About</h3>
        </div>

        <div class="about-header-main-subtitle">
          <figure><img src="../img/about/about-main.jpg" srcset="../img/about/about-main.jpg 1x,../img/about/about-main@2x.jpg 2x" alt="about-main"></figure>
          <div class="subtitle-box">
            <p class="concept">Support your web service</p>
            <p class="concept-text">webを通して人々の心や生活が豊かになるよう、法律の面からサポートいたします。</p>            </div>
        </div>
      </div>
    </header>

    <main>
      <section class="p-point">
        <div class="point-box">
          <div class="point-box-inner">
            <div class="point-title">
            <p class="point-title-main">Point</p>
            <p class="point-title-sub"> 制作会社での経歴を活かしたご提案ができます</p>
          </div>

          <div class="point-text-box">
            <div class="c-text-box">
              <p>私はこれまでにIT系企業においてweb制作、新規webサービスの立ち上げ、
                 ファンクラブビジネスを経験し「webが持つ可能性」を信じるようになりました。</p>
              <p>「webが持つ可能性」とはwebを通して人々の心や生活がもっと豊かになっていくことです。</p>
              <br>
              <br>
              <p>当事務所ではIT法務（契約書作成や各種許認可手続きなど）を中心に、海外技術者のビザ
                 手続き、そしてwebを介した新たなビジネスモデルとして評判の民泊（登録手続き）など、
                 広くwebに関する手続きのサポートを行ってまいります。</p>
              <br>
              <br>
            </div>

            <div class="c-text-box">
              <p>書類作成など法律面からあなたのwebサービスのサポートをします。</p>
              <p>新たなwebサービスが生まれ、そのサービスを利用することで多くの人々の心や生活が豊かになる。</p>
              <p>そしていつか日本が世界のIT分野をリードする日がくることを願っています。</p>
            </div>
          </div>
        </div>
      </section>

      <section class="p-profile">
        <div class="profile-box">
          <div class="profile-box-inner">
            <div class="office-data">
              <p class="office-data-title">Office data</p>

              <dl class="office-data-list">
                <dt class="list-menu">名称:</dt><dd class="detail">藤岡行政書士事務所</dd>
                <dt class="list-menu">代表:</dt><dd class="detail">藤岡雅也</dd>
                <dt class="list-menu">行政書士番号:</dt><dd class="detail">0000000000000</dd>
                <dt class="list-menu">取り扱い案件:</dt><dd class="detail">労務、民泊、Visa</dd>
                <dt class="list-menu">電話番号:</dt><dd class="detail">06-5555-5555</dd>
                <dt class="list-menu">FAX:</dt><dd class="detail">06-5555-5555</dd>
                <dt class="list-menu">住所</dt>
              </dl>
            </div>

            <figure><img src="../img/about/nice-guy.jpg" srcset="../img/about/nice-guy.jpg 1x,../img/about/nice-guy@2x.jpg 2x" alt="nice-guy"></figure>
          </div>
        </div>
      </section>

      <section class="map">
        <?php include("../php/ggmap.php"); ?>
      </section>

      <section class="p-contact">
        <?php include("../php/contact.php"); ?>
      </section>
    </main>

    <footer>
      <?php include("../php/footer.php"); ?>
    </footer>
  </body>

</html>
