$(function(){
  //google map API setting
  var map;

  function initMap(){
    let fujioka = {lat:35.017244 ,lng:135.756407};
    map = new google.maps.Map(document.getElementById('map'), {
        center: fujioka,
        zoom: 18.15
    });

  //maker setting .index
  var marker = new google.maps.Marker({
    position: fujioka,
    map: map,
    title: 'fujioka',
    icon: {
        url: "./img/index/pin.png",
        scaledSize: new google.maps.Size(150,150 * 1.13) //.png 170*192
      }
    });

  //maker setting .about
  var marker = new google.maps.Marker({
    position: fujioka,
    map: map,
    title: 'fujioka',
    icon: {
        url: "../img/about/pin.png",
        scaledSize: new google.maps.Size(150,150 * 1.13) //.png 170*192
      }
    });

  //grayscale setting
  var mapStyle = [{
    "stylers": [{
    "saturation": -100
  }]
}];
  var mapType = new google.maps.StyledMapType(mapStyle);
    map.mapTypes.set( 'GrayScaleMap', mapType);
    map.setMapTypeId( 'GrayScaleMap' );
  }

  //start
  window.onload = function () {
    initMap();
  }
});

console.log(marker);
