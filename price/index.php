<!DOCTYPE html>
<html>
  <head>
    <title>fujioka-office</title>
    <meta charset="utf-8">
    <!--viewport-->
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--OGP common setting-->
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:url" content="******">
    <meta property="og:title" content="fujioka-office">
    <meta property="og:type" content="website">
    <meta property="og:description" content="藤岡事務所のオフィシャルサイトです。">
    <meta property="og:image" content="******">
    <!--.css road-->
    <link href="../stylesheet.min.css" type="text/css" rel="stylesheet">
    <!--JQuery road-->
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <!--JQuery UI road-->
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
  </head>

  <body>
    <header>
      <div class="l-header-top">
        <?php include("../php/header-top.php"); ?>
      </div>

      <div class="c-header-title">
        <h3>Price</h3>
      </div>

      <div class="price-menu">
        <div class="price-menu-innder">
          <div class="price-button">
            <p>IT法務</p>
            <div class="arrow-bottom"></div>
          </div>

          <div class="price-button">
            <p>ビザ申請業務</p>
            <div class="arrow-bottom"></div>
          </div>

          <div class="price-button">
            <p>民泊申請</p>
            <div class="arrow-bottom"></div>
          </div>
        </div>
      </div>
    </header>

    <main>
      <section class="p-price">
        <div class="price-list-box">
          <div class="price-list-module table-scroll">
            <div class="service-title">
              <p class="service-number">#01</p>
              <p class="service-name">IT法務</p>
            </div>

            <p>IT法務顧問</p>
            <div class="c-text-box">
              <p>IT法務顧問など継続的にリーガルチェック、法務サポートを行います。</p>
            </div>

            <table class="price-01">
              <tr><th class="row-1">業務内容</th><th class="row-2">報酬額</th><th class="row-3">備考</th></tr>
              <tr><td class="row-1">&nbsp;</td><td class="row-2">&nbsp;</td><td class="row-3">&nbsp;</td></tr>
              <tr><td class="row-1">&nbsp;</td><td class="row-2">&nbsp;</td><td class="row-3">&nbsp;</td></tr>
              <tr><td class="row-1">&nbsp;</td><td class="row-2">&nbsp;</td><td class="row-3">&nbsp;</td></tr>
              <tr><td class="row-1">&nbsp;</td><td class="row-2">&nbsp;</td><td class="row-3">&nbsp;</td></tr>
              <tr><td class="row-1">&nbsp;</td><td class="row-2">&nbsp;</td><td class="row-3">&nbsp;</td></tr>
            </table>

            <p>研修</p>
            <div class="c-text-box">
              <p>企業研修を実施いたします。教育内容などご相談に応じます。</p>
            </div>
          </div>

          <div class="price-list-module table-scroll">
            <div class="service-title">
              <p class="service-number">#02</p>
              <p class="service-name">ビザ申請業務</p>
            </div>

            <div class="c-text-box">
              <p>IT法務顧問継続的にリーガルチェック、法務サポートを行います。</p>
            </div>

            <table class="price-02">
              <tr><th class="row-1">業務内容</th><th class="row-2">報酬額</th><th class="row-3">備考</th></tr>
              <tr><td class="row-1">&nbsp;</td><td class="row-2">&nbsp;</td><td class="row-3">&nbsp;</td></tr>
              <tr><td class="row-1">&nbsp;</td><td class="row-2">&nbsp;</td><td class="row-3">&nbsp;</td></tr>
              <tr><td class="row-1">&nbsp;</td><td class="row-2">&nbsp;</td><td class="row-3">&nbsp;</td></tr>
              <tr><td class="row-1">&nbsp;</td><td class="row-2">&nbsp;</td><td class="row-3">&nbsp;</td></tr>
              <tr><td class="row-1">&nbsp;</td><td class="row-2">&nbsp;</td><td class="row-3">&nbsp;</td></tr>
            </table>
          </div>

          <div class="price-list-module">
            <div class="service-title">
              <p class="service-number">#03</p>
              <p class="service-name">民泊業務</p>
            </div>

            <p>住宅宿泊事業届等の申請</p>
            <p>色々なご提案ができます。直接当社までお問い合わせください直接お問い合わせください</p>
          </div>
        </div>
      </section>

      <section class="p-contact">
        <?php include("../php/contact.php"); ?>
      </section>
    </main>

    <footer>
      <?php include("../php/footer.php"); ?>
    </footer>
  </body>
</html>
