<!DOCTYPE html>
<html>
  <head>
    <title>fujioka-office</title>
    <meta charset="utf-8">
    <!--viewport-->
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--OGP common setting-->
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:url" content="******">
    <meta property="og:title" content="fujioka-office">
    <meta property="og:type" content="website">
    <meta property="og:description" content="藤岡事務所のオフィシャルサイトです。">
    <meta property="og:image" content="******">
    <!--.css road-->
    <link href="../stylesheet.min.css" type="text/css" rel="stylesheet">
    <!--JQuery road-->
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <!--JQuery UI road-->
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
  </head>

  <body>
    <header>
      <div class="l-header-top">
        <?php include("../php/header-top.php"); ?>
      </div>

      <div class="c-header-title">
        <h3>Blog</h3>
      </div>
    </header>

    <main>
      <section class="p-blog">
        <div class="blog-list-box">
          <ul class="blog-list-box-inner">
            <li class="c-blog-module">
              <?php include("../php/blog-article-20191111.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191110.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191109.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191111.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191110.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191109.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191111.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191110.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191109.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191111.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191110.php"); ?>
            </li>

            <li class="c-blog-module">
              <?php include("../php/blog-article-20191109.php"); ?>
            </li>
          </ul>
        </div>

        <div class="c-pager">
          <ol class="pagination">
            <li class="pre"><a href="#"><span>&lt;</span></a></li>
            <li><a href="#" class="active"><span>1</span></a></li>
            <li><a href="#"><span>2</span></a></li>
            <li><a href="#"><span>3</span></a></li>
            <li><a href="#"><span>4</span></a></li>
            <li><a href="#"><span>5</span></a></li>
            <li class="next"><a href="#"><span>&gt;</span></a></li>
          </ol>
        </div>
      </section>
    </main>

    <footer>
      <?php include("../php/footer.php"); ?>
    </footer>
  </body>
</html>
