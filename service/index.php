<!DOCTYPE html>
<html>
  <head>
    <title>fujioka-office</title>
    <meta charset="utf-8">
    <!--viewport-->
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--OGP common setting-->
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:url" content="******">
    <meta property="og:title" content="fujioka-office">
    <meta property="og:type" content="website">
    <meta property="og:description" content="藤岡事務所のオフィシャルサイトです。">
    <meta property="og:image" content="******">
    <!--.css road-->
    <link href="../stylesheet.min.css" type="text/css" rel="stylesheet">
    <!--JQuery road-->
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <!--JQuery UI road-->
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
  </head>

  <body>
    <header>
      <div class="l-header-top">
        <?php include("../php/header-top.php"); ?>
      </div>

      <div class="c-header-title">
        <h3>Service</h3>
      </div>

      <figure class="header-photo"><img src="../img/service/service-main.jpg" srcset="../img/service/service-main.jpg 1x,../img/service/service-main@2x.jpg 2x" alt="service-main"></figure>
    </header>

    <main>
      <section class="p-service">
        <div class="service-list-box">
          <div class="service-list-box-inner">
            <div class="service-list-box-module">
              <div class="module-left">
                <div class="service-title">
                  <p class="service-number">#01</p>
                  <p class="service-name">IT法務</p>
                </div>

                <div class="c-text-box">
                  <p>IT関連の契約書には紛争を予防するための「予防法務」としてとても大きな役割があり、
                     サービスの内容をよく理解した上で作成をする必要があります。</p>
                  <p>また知的財産権の管理など一般的な契約書とは違った内容を盛り込むことも必要です。</p>
                  <br>
                  <br>
                  <p>当事務所ではweb業界に8年間身を置き、webサイトの制作·会員管理システムの運用·新
                     規webサービスの立ち上げに従事した経験を持つ行政書士が契約書のチェック·作成をお
                     手伝いいたします。</p>
                </div>
              </div>

              <div class="module-right">
                <div class="prepare-box">
                  <figure class="reverse"><img src="../img/service/“.svg" alt="double-quotation"></figure>
                  <p>ご用意するサービス</p>
                  <figure><img src="../img/service/“.svg" alt="double-quotation"></figure>
                </div>

                <div class="prepare-list">
                  <ul>
                    <li>IT法務顧問サービス（月5,000円～）</li>
                    <li>契約書リスク診断</li>
                    <li>契約書ドラフト作成</li>
                    <li>IT法務研修など</li>
                  </ul>
                </div>

                <div class="c-blue-button" type="button">
                  <a>料金ページへ</a>
                </div>
              </div>
            </div>

            <div class="service-list-box-module">
              <div class="module-left">
                <div class="service-title">
                  <p class="service-number">#02</p>
                  <p class="service-name">ビザ申請業務</p>
                </div>

                <div class="c-text-box">
                  <p>数多くの外国人IT技術者の在留資格申請業務に従事した経験を元に、</p>
                  <p>当事務所では在留資格、永住申請・定住申請・帰化申請などの手続きを行ってまいります。</p>
                  <p>優秀な外国人IT技術者を受け入れ、業界のサービス向上の一助ができるようサポートいたします。</p>
                </div>
              </div>

              <div class="module-right">
                <div class="prepare-box">
                  <figure class="reverse"><img src="../img/service/“.svg" alt="double-quotation"></figure>
                  <p>ご用意するサービス</p>
                  <figure><img src="../img/service/“.svg" alt="double-quotation"></figure>
                </div>

                <div class="prepare-list">
                  <ul>
                    <li>ビザ取得・更新</li>
                    <li>永住許可</li>
                    <li>帰化申請</li>
                    <li>国籍取得など</li>
                  </ul>
                </div>

                <div class="c-blue-button" type="button">
                  <a>料金ページへ</a>
                </div>
              </div>
            </div>

            <div class="service-list-box-module">
              <div class="module-left">
                <div class="service-title">
                  <p class="service-number">#03</p>
                  <p class="service-name">民泊申請</p>
                </div>

                <div class="c-text-box">
                  <p>住宅宿泊事業法（いわゆる民泊新法）が2018年6月15日に施行されます。</p>
                  <p>年々増加する訪日外国人旅行者の宿泊の受け皿として、今後民泊は全国で解禁され、各地で営業が行われます。</p>
                  <p>2020年の東京オリンピック開催に向けてますます広がっていく民泊。</p>
                  <p>当事務所では最新のwebサービスでもある民泊の普及に注力します。</p>
                  <p>民泊登録の申請（住宅宿泊事業届等）をサポートし、民泊仲介サイトへの登録や情報提供などでお手伝いをいたします。</p>
                </div>
              </div>

              <div class="module-right">
                <div class="suggestion">
                  <p>色々なご提案ができます。</p>
                  <p>直接当社までお問い合わせください</p>
                </div>

                <div class="c-blue-button" type="button">
                  <a>お問い合わせ</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="p-contact">
        <?php include("../php/contact.php"); ?>
      </section>
    </main>

    <footer>
      <?php include("../php/footer.php"); ?>
    </footer>
  </body>

  </html>
