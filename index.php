<!DOCTYPE html>
<html>
  <head>
    <title>fujioka-office</title>
    <meta charset="utf-8">
    <!--viewport-->
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--OGP common setting-->
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:url" content="******">
    <meta property="og:title" content="fujioka-office">
    <meta property="og:type" content="website">
    <meta property="og:description" content="藤岡事務所のオフィシャルサイトです。">
    <meta property="og:image" content="******">
    <!--.css road-->
    <link href="./stylesheet.min.css" type="text/css" rel="stylesheet">
    <!--JQuery road-->
    <script type="text/javascript" src="./js/jquery-3.4.1.min.js"></script>
    <!--JQuery UI road-->
    <script type="text/javascript" src="./js/jquery-ui.min.js"></script>
    <!--ggmap road-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzvCwkJdwx4Rl4uDuxP6Lzgtfgs0kkptM&callback=initMap" async defer></script>
    <script type="text/javascript" src="./js/ggmap.js"></script>
    <!---swiper.js--->
    <script src="./js/swiper.min.js"></script>
    <!-- jsファイルはhtml読み込み後に実行 -->
    <script type="text/javascript" src="./js/script.js"></script>
  </head>

  <body>
    <header>
      <div class="l-header-top">
        <?php include("php/header-top.php"); ?>
      </div>

      <div class="header-main">
        <div class="title-box">
          <h1><img src="./img/index/title.png" alt="WEB INTEGRATION & LEGAL AFFAIRS"></h1>
          <p>IT専門家の行政書士たちが、あなたのビジネスを守ります。</p>
        </div>

        <figure class="sp-main"><img src="./img/index/index-spmain.jpg" srcset="./img/index/index-spmain.jpg 1x,./img/index/index-spmain@2x.jpg 2x" alt="index-spmain"></figure>

        <!-- Slider main container -->
        <div class="swiper-container">
          <!-- Additional required wrapper -->
          <div class="swiper-wrapper">
            <!-- Slides -->
            <div class="swiper-slide"><img src="./img/index/swiper-1.jpg" srcset="./img/index/swiper-1.jpg 1x,./img/index/swiper-1@2x.jpg 2x" alt="swiper1"></div>
            <div class="swiper-slide"><img src="./img/index/swiper-2.jpg" srcset="./img/index/swiper-2.jpg 1x,./img/index/swiper-2@2x.jpg 2x" alt="swiper2"></div>
            <div class="swiper-slide"><img src="./img/index/swiper-3.jpg" srcset="./img/index/swiper-3.jpg 1x,./img/index/swiper-3@2x.jpg 2x" alt="swiper3"></div>
          </div>
          <!-- If we need pagination -->
          <div class="swiper-pagination"></div>
          <!-- If we need navigation buttons -->
          <div class="swiper-button-prev swiper-button-white"></div>
          <div class="swiper-button-next swiper-button-white"></div>
          <!-- If we need scrollbar -->
          <div class="swiper-scrollbar"></div>
        </div>

        <div class="news">
          <article class="news-box">
            <div class="news-box-inner">
              <h2>NEWS</h2>
              <ol>
                <li>
                  <time datetime="2018-01-11">2018/01/11</time>
                  <p>ああああああああああああああああああああああああああああああああ</p>
                </il>
              </ol>
            </div>
          </article>
        </div>

        <div class="blue-box">
          <div class="bulu-box-inner">
            <p class="blue-box-text1">webを通して人々の心や生活が豊かになるよう、法律の面からサポートいたします。</p>
            <p class="blue-box-text2">当事務所ではIT法務（契約書作成や各種許認可手続きなど）を中心に、
            海外技術者のビザ手続き、そしてwebを介した新たなビジネスモデルとして評判の民泊（登録手続き）
            など、広くwebに関する手続きのサポートを行ってまいります。</p>
          </div>
        </div>
      </div>
    </header>

    <main>
      <section class="p-index-service">
        <h3>SERVICE</h3>

        <ul class="service-box">
          <li class="service-box-module">
            <div>
              <figure><img src="./img/index/it.jpg" srcset="./img/index/it.jpg 1x,./img/index/it@2x.jpg 2x" alt="it"></figure>
              <div class="service-title">
                <p class="service-number">#01</p>
                <p class="service-name">IT法務</p>
              </div>
            </div>
            <p class="service-text">簡単な説明簡単な説明簡単な説明簡単な説明簡単な説明簡単な説明簡単な説明簡単な</p>
          </li>

          <li class="service-box-module">
            <div>
              <figure><img src="./img/index/Immigration.jpg" srcset="./img/index/Immigration.jpg 1x,./img/index/Immigration@2x.jpg 2x" alt="Immigration"></figure>
              <div class="service-title">
                <p class="service-number">#02</p>
                <p class="service-name">入管業務</p>
              </div>
            </div>
            <p class="service-text">簡単な説明簡単な説明簡単な説明簡単な説明簡単な説明簡単な説明簡単な説明簡単な</p>
          </li>

          <li class="service-box-module">
            <div>
              <figure><img src="./img/index/Airbnb.jpg" srcset="./img/index/Airbnb.jpg 1x,./img/index/Airbnb@2x.jpg 2x" alt="Airbnb"></figure>
              <div class="service-title">
                <p class="service-number">#03</p>
                <p class="service-name">民泊申請</p>
              </div>
            </div>
            <p class="service-text">簡単な説明簡単な説明簡単な説明簡単な説明簡単な説明簡単な説明簡単な説明簡単な</p>
          </li>
        </ul>

        <div class="c-blue-button" type="button">
          <a>詳しく見る</a>
        </div>
      </section>

      <section class="p-index-blog">
        <h3>BLOG</h3>

        <ul class="blog-box">
          <li class="c-blog-module">
            <?php include("php/blog-article-20191111.php"); ?>
          </li>

          <li class="c-blog-module">
            <?php include("php/blog-article-20191110.php"); ?>
          </li>

          <li class="c-blog-module">
            <?php include("php/blog-article-20191109.php"); ?>
          </li>

          <li class="c-blog-module">
            <?php include("php/blog-article-20191111.php"); ?>
          </li>

          <li class="c-blog-module">
            <?php include("php/blog-article-20191110.php"); ?>
          </li>

          <li class="c-blog-module">
            <?php include("php/blog-article-20191109.php"); ?>
          </li>
        </ul>

        <div class="c-blue-button" type="button">
          <a>一覧を見る</a>
        </div>
      </section>

      <section class="p-ggmap">
        <?php include("php/ggmap.php"); ?>
      </section>

      <section class="p-contact">
        <?php include("php/contact.php"); ?>
      </section>
    </main>

    <footer>
      <?php include("php/footer.php"); ?>
    </footer>
  </body>
</html>
